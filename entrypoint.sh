#!/bin/sh
LPUSERNAME="$1"

lpass login $LPUSERNAME >/dev/null 2>&1;
touch results.csv;

for ID in `lpass ls | sed -r "s/^.*id: ([0-9]+)]/\\1/"`; do
  CONTENT=`lpass show --all $ID`
  USER=`printf "%s" "$CONTENT" | grep "Username: " | sed -r "s/Username: (.*)$/\\1/"`
  URL=`printf "%s" "$CONTENT" | grep "URL: " | sed -r "s/URL: (.*)$/\\1/"`
  printf "%s,%s\n" "$USER" "$URL" >> results.csv
done

lpass logout -f >/dev/null 2>&1;

cat results.csv

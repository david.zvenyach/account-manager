# Account Manager

A simple webservice that allows a user to create a list of all accounts and usernames (BUT NOT PASSWORDS) associated with the password manager.

## Proposed flow
1. User goes to secure website. Selects whether uses lastpass or 1password.
2. If lastpass, asks user to enter username, master password, and (if applicable) 2FA.
3. Webservice takes the information. Spits back CSV with accounts/usernames.

Will plan to use Docker to not persist any data.

## Usage

```
docker build . -t lp
docker run -ti lp [$USER_EMAIL_ADDRESS] >> results.csv
```

FROM centos:7
RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
RUN yum install -y lastpass-cli
COPY entrypoint.sh /
ENTRYPOINT ["./entrypoint.sh"]
